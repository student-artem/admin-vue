"use strict";

const { src, dest, parallel, series, watch } = require('gulp');
const source     = require("vinyl-source-stream");
const browserify = require("browserify");
const sass       = require("gulp-sass");
const cleancss   = require('gulp-clean-css');
const concat     = require('gulp-concat');

let dist = "C:/Main/OSPanel/domains/study"

function scripts() {
    return browserify('./app/js/main.js', { debug: true })
    .transform("babelify", {
        presets: [ "@babel/preset-env" ],
        sourceMap: true
    }).bundle()
    .pipe(source('./main.min.js'))
    .pipe(dest('./app/js'))
}

function styles() {
    return src("./app/scss/**/*.scss")
    .pipe(sass().on('error', sass.logError))
    .pipe(cleancss(({ level: { 1: { specialComments: 0 } }, /*format: 'beautify'*/ })))
    .pipe(concat('main.min.css'))
    .pipe(dest('./app/scss'))
}

function buildcopy() {
    return src([
        '{app/js,app/css}/*.min.*', 'app/*.html', 'app/api/**.*'
    ], { base: 'app/' })
    .pipe(dest(dist) + '/admin/')
}

function startwatch() {
    watch([ `./app/scss/**/*`, '!./app/scss/**/*.min.css' ], { usePolling: true }, styles, buildcopy)
    watch([ './app/js/**/*.js', '!./app/js/**/*.min.js' ], { usePolling: true }, scripts, buildcopy)
    watch([ './app/*.html'], { usePolling: true }, buildcopy)
}

exports.scripts = scripts
exports.styles  = styles
exports.default = series(scripts, styles, parallel(startwatch))



